#include "go.h"
#include "randomai.h"
#include <iostream>
#include"list"


namespace mylib {
namespace go {



 Block::Block(std::shared_ptr<StoneColor> stone)
{
    //_color = StoneColor->_color;
    std::vector<Point> _points;
/*
    if (stone->has_north() && (stone->_board->stone(stone->north()))==stone->_color)
    {
       addBlock(std::shared_ptr<Block>(this));
       _points.push_back(stone->north());
    }
    if(stone->has_south() && (stone->_board->stone(stone->south()))==stone->_color)
    {
        addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->south());
    }
    if(stone->has_east() && (stone->_board->stone(stone->east()))==stone->_color)
    {
        addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->east());
    }
    if(stone->has_west() && (stone->_board->stone(stone->west()))==stone->_color)
    {
        addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->west());
    }
   */
}

 void Block::joinBlock(std::shared_ptr<Block> newblock, std::shared_ptr<Block> existingblock)
 {
     if (newblock==existingblock)
         return ;
     newblock-> addBlock(existingblock);
     _block.remove(existingblock);
 }

 std::set<Point> Block::getPoint() const
 {
     return this->_stone;
 }


 void Block::addBlock(std::shared_ptr<Block> block)
 {
     _block.push_back(block);
 }
 void Block::addPoint(Point intersection)
 {
   _stone.insert(intersection);
 }

 namespace priv {

    Board_base::Board_base( Size size ) {
      resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
      // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass} {}


    void
    Board_base::resetBoard(Size size) {

      _current.board.clear();
      _size = size;
      _current.turn = StoneColor::White;
    }

    Size
    Board_base::size() const {
      return _size;
    }

    bool
    Board_base::wasPreviousPass() const {

      return _current.was_previous_pass;
    }

    StoneColor
    Board_base::turn() const {
    return _current.turn == StoneColor::Black ? StoneColor::White: StoneColor::Black;
    }

  }

  void
  Board::placeStone(Point intersection)
  {
      if(!this->hasStone(intersection))
      {
          _current.board[intersection] = turn();
          _current.turn = turn();
      }



  /*    if ((a.has_East))
      {
            this->stone(a.East);
      }*/
  }

  void
  Board::passTurn() {
    _current.turn = turn();
  }

  bool
  Board::hasStone(Point intersection) const
  {
    return _current.board.count(intersection);
  }

  StoneColor
  Board::stone(Point intersection) const {

    return _current.board.at(intersection);
  }


  bool
  Board::isNextPositionValid(Point /*intersection*/) const {

    return true;
  }

  Engine::Engine()
    : _board{}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

  void
  Engine::newGame(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
  }

  void
  Engine::newGameAiVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass) {

    _board = Board {std::forward<Board::BoardData>(board),turn,was_previous_pass};

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  const Board&
  Engine::board() const {

    return _board;
  }

  const GameMode&
  Engine::gameMode() const {

    return _game_mode;
  }

  StoneColor
  Engine::turn() const {

    return _board.turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const {

    if(      turn() == StoneColor::Black ) return _black_player;
    else if( turn() == StoneColor::White ) return _white_player;
    else                              return nullptr;
  }

  void
  Engine::placeStone(Point intersection) {

    _board.placeStone(intersection);
  }

  void
  Engine::passTurn() {

    if(board().wasPreviousPass()) {
      _active_game = false;
      return;
    }

    _board.passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {

    if( currentPlayer()->type() != PlayerType::Ai) return;

    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
      placeStone( p->nextStone() );
    else
      passTurn();
  }

  bool
  Engine::isGameActive() const {

    return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const {

    return _board.isNextPositionValid(pos);
  }

  //start
  //--------------------//

namespace priv
{
  bool Stone_Base::has_North() const
  {
    return (*(this->_board)).hasStone(Point(_point.first+1,_point.second));
  }

  bool Stone_Base::has_South() const
  {
    return (*(this->_board)).hasStone(Point(_point.first-1,_point.second));
  }

  bool Stone_Base::has_East() const
  {
    return (*(this->_board)).hasStone(Point(_point.first,_point.second+1));
  }

  bool Stone_Base::has_West() const
  {
    return (*(this->_board)).hasStone(Point(_point.first,_point.second-1));
  }
}
} // END namespace go
} // END namespace mylib
