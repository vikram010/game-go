#ifndef GO_H
#define GO_H

//stl
#include <map>
#include <memory>
#include <utility>
#include <chrono>
#include<vector>
#include<Set>
#include<list>

namespace mylib {

namespace go {

  class Board;
  struct GameState;

  class Player;
  class AiPlayer;
  class HumanPlayer;

  class Engine;
  class Stone_Base;
  class Boundry;
  class Block;

  using time_type = std::chrono::duration<int,std::milli>;

  constexpr time_type operator"" _ms (unsigned long long int milli) { return time_type(milli); }
  constexpr time_type operator"" _s  (unsigned long long int sec)   { return time_type(sec*1000); }

  enum class StoneColor {
    White       = 0,
    Black       = 1
  };

  enum class PlayerType {
    Human       = 0,
    Ai          = 1
  };

  enum class GameMode {
    VsPlayer    = 0,
    VsAi        = 1,
    Ai          = 2
  };

  using Point   = std::pair<int,int>;
  using Size    = std::pair<int,int>;

  //-- Start of Class Block

  class Block{
      std::set<Point> _stone;
      std::set<Boundry> _boundry;
      StoneColor _color;
      public:
           Block(std::shared_ptr<StoneColor> stone);
           std::list<std::shared_ptr <Block>> _block;

           std::list<std::shared_ptr <Block>> _getblock;

           std::set<Point> getPoint()const;
           void addPoint(Point intersection);

           std::shared_ptr<Block> findBlock(Point intersection) const;
           void addBlock(std::shared_ptr<Block> block);

           void joinBlock(std::shared_ptr<Block> _block);
           void joinBlock(std::shared_ptr<Block> newBlock, std::shared_ptr<Block> existingblock);
  };

  class Boundry{
  public:
       Boundry(std::vector<Point> points);
       std::vector<Point> _points;
         bool operator < ( const Boundry&  ) const
         {
             return true;
         }
  };

   using BoardData = std::map<Point,Stone_Base>;

  // INVARIANTS:
  //   Board fullfills; what does one need to know in order to consider a given position.
  //   * all stones and their position on the board
  //   * who places the next stone
  //   * was last move a pass move
  //   * meta: blocks and freedoms

  namespace priv {

  class Board_base {
    public:
      using BoardData = std::map<Point,StoneColor>;

      struct Position {
        BoardData     board;
        StoneColor         turn;
        bool          was_previous_pass;

        Position () = default;
        explicit Position ( BoardData&& data, StoneColor turn, bool was_previous_pass);

      }; // END class Position


      // Constructors
      Board_base() = default;
      explicit Board_base( Size size );
      explicit Board_base( BoardData&& data, StoneColor turn, bool was_previous_pass );



      // Board data
      Size             size() const;
      bool             wasPreviousPass() const;
      StoneColor            turn() const;

      // Board
      void             resetBoard( Size size );

      // Board
      Size             _size;
      Position         _current;

    }; // END base class Board_base


    class Stone_Base{
    public:
        StoneColor _colour;
        Point _point;
        std::shared_ptr<Board> _board;
        Stone_Base()=default;

        explicit Stone_Base(StoneColor _colour,Point _point,std::shared_ptr<Board> _board);

        bool has_North() const;
        bool has_South() const;
        bool has_East() const;
        bool has_West() const;
        Point North() const;
        Point East() const;
        Point West() const;
        Point South() const;
    };//End Stone Base Class

 } // END "private" namespace priv


  class stonecolor : private priv :: Stone_Base
  {
    public:
      using Stone_Base::East;
      using Stone_Base::West;
      using Stone_Base::North;
      using Stone_Base::South;
      using Stone_Base::Stone_Base;
      using Stone_Base::_board;
      using Stone_Base::_point;

      using Stone_Base::_colour;
      using Stone_Base::has_East;
      using Stone_Base::has_West;
      using Stone_Base::has_North;
      using Stone_Base::has_South;

  };



  class Board : private priv::Board_base {
  public:
    // Enable types
    using Board_base::BoardData;

    using Board_base::Position;

    // Enable constructors
    using Board_base::Board_base;

    // Make visible from Board_base
    using Board_base::resetBoard;
    using Board_base::size;
    using Board_base::wasPreviousPass;
    using Board_base::turn;

    // Validate
    bool                  isNextPositionValid( Point intersection ) const;

    // Actions
    void                  placeStone( Point intersection );
    void                  passTurn();

    // Stones and positions
    bool                  hasStone( Point intersection ) const;
    StoneColor                 stone( Point intersection ) const;


  }; // END class Board

  class Engine : public std::enable_shared_from_this<Engine> {
  public:
    Engine();

    void                            newGame( Size size );
    void                            newGameVsAi( Size size );
    void                            newGameAiVsAi( Size size );
    void                            newGameFromState( Board::BoardData&& board, StoneColor turn,
                                                      bool was_previous_pass );

    const Board&                    board() const;

    StoneColor                           turn() const;

    const GameMode&                 gameMode() const;
    const std::shared_ptr<Player>   currentPlayer() const;

    void                            placeStone( Point intersection );
    bool                            validateStone( Point intersection ) const;
    void                            passTurn();

    void                            nextTurn( time_type think_time = 100_ms );
    bool                            isGameActive() const;


  private:
    Board                           _board;

    GameMode                        _game_mode;
    bool                            _active_game;

    std::shared_ptr<Player>         _white_player;
    std::shared_ptr<Player>         _black_player;

  }; // END class Engine

} // END namespace go

} // END namespace mylib

#endif //GO_H
